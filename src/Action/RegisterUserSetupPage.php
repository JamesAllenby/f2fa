<?php
namespace JAllenby37;

/**
 * The RegisterUserSetup class
 * 
 * This class is responsible for guiding the user in activating two-factor
 * authentication on their account.
 */
class RegisterUserSetupPage {

    /** @var \Twig\Environment Twig object */
    private $twig;

    /**
     * RegisterUserSetup constructor
     */
    public function __construct(\Twig\Environment $twig) {
        $this->twig = $twig;
    }

    /**
     * RegisterUserSetup entrypoint
     */
    public function run() {
        // Get the currently logged in user
        $user = \wp_get_current_user();

        // Handle the post request for this user ID
        $this->handle_post($user->ID);

        // Secret expiry state
        $expired = FALSE;

        // Get the pending secret expiry time
        $secret      = \get_user_meta($user->ID, 'f2fa_pending_secret', TRUE);
        $expiry_time = \get_user_meta($user->ID, 'f2fa_pending_secret_expiry', TRUE);

        // If expiry time or secret is empty, then set expired to TRUE
        if (!\strlen($expiry_time) || !\strlen($secret))
            $expired = TRUE;

        // If the expiry time has been reached, then set expired to TRUE
        if (\time() > (int)$expiry_time)
            $expired = TRUE;

        // If the pending secret has expired, regenerate another
        if ($expired) {
            $this->regenerate_pending_secret($user->ID);
        }

        \add_submenu_page(
            null,                             // Parent slug (null means don't show anywhere)
            'F2FA Setup',                     // Page title
            'F2FA Setup',                     // Menu title (unused)
            'read',                           // Capability required
            'f2fa-setup',                     // URL slug
            array($this, 'render_user_setup') // Callback function
        );
    }

    /**
     * Regenerates a pending secret for a particular user
     * 
     * @param int uid User ID
     */
    public function regenerate_pending_secret(int $uid) {
        // Clear the pending secret and its expiry time
        \delete_user_meta($uid, 'f2fa_pending_secret');
        \delete_user_meta($uid, 'f2fa_pending_secret_expiry');

        // New expiry time for one-hour in the future and a new
        // base32 encoded shared secret
        $expiry_time = time() + (60 * 60);
        $secret      = OTP::GenerateSecret();

        // Add the new secret and expiry time to the users meta table
        \add_user_meta($uid, 'f2fa_pending_secret', $secret, TRUE);
        \add_user_meta($uid, 'f2fa_pending_secret_expiry', $expiry_time, TRUE);
    }

    /**
     * 
     */
    public function handle_post($uid) {
        // Check if an OTP code was submitted for verification
        if (!\array_key_exists('setup_otp', $_POST))
            return;

        // Get this users pending secret
        $secret = \get_user_meta($uid, 'f2fa_pending_secret', TRUE);
        
        // Create a new TOTP object from the secret
        $totp = new TOTP($secret);

        // Ensure the time is up-to-date for the TOTP object
        $totp->reset_time();

        // Compare if the generated OTP code is the same as the supplied
        if ($totp->get_otp() == $_POST['setup_otp']) {
            
            // Replace the active secret with the newly verified one
            \delete_user_meta($uid, 'f2fa_active_secret');
            \delete_user_meta($uid, 'f2fa_active_date');
            \add_user_meta($uid, 'f2fa_active_secret', $secret, TRUE);
            \add_user_meta($uid, 'f2fa_active_date', time(), TRUE);

            // Regenerate the secret as we just activated the current one
            $this->regenerate_pending_secret($uid);

            // Redirect back to the admin page
            \wp_redirect(admin_url());
        }
    }

    public function render_user_setup() {
        // Get the currently logged in user
        $user = \wp_get_current_user();

        // Get the secret from the user ID
        $username    = $user->data->user_login;
        $email       = $user->data->user_email;
        $issuer      = \get_bloginfo();
        $secret      = \get_user_meta($user->ID, 'f2fa_pending_secret', TRUE);
        $expiry_time = \get_user_meta($user->ID, 'f2fa_pending_secret_expiry', TRUE);

        echo $this->twig->render('setup_otp.html', array(
            'email'       => $email,
            'secret'      => \strtoupper($secret),
            'issuer'      => $issuer,
            'expiry_min' => \floor(($expiry_time - time()) / 60) . ' minutes'
        ));
    }
}