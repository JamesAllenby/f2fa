<?php
namespace JAllenby37;

/**
 * The RegisterSettings class
 * 
 * This class is responsible for registering the settings for the F2FA admin interface
 * for later use by a DOM render and POST request saving.
 */
class RegisterSettings {

    /** @var \Twig\Environment Twig object */
    private $twig;

    /**
     * RegisterSettings constructor
     * 
     * @param \Twig\Environment twig
     */
    function __construct(\Twig\Environment $twig) {
        $this->twig = $twig;
    }
    
    /**
     * RegisterSettings entrypoint
     * 
     * @return void
     */
    public function run(): void {
        /**
         * An array of maps indicating all the types of settings that
         * should be present to administrators.
         *          id: Unique ID of the setting, used as name in Database
         *        name: Formatted name for display in admin interfaces
         * description: A description of the setting and what it does
         *        type: Type of variable (boolean, string,  etc)
         *     default: The default value for this setting
         */
        $settings = array(
            array(
                'id'          => 'f2fa_pedantic',
                'name'        => 'Pedantic Mode',
                'description' => 'This setting improves security by enabling strict policies on logins.',
                'type'        => 'boolean',
                'default'     => '0'
            ),
            array(
                'id'          => 'f2fa_lockouts',
                'name'        => 'Enable lock-outs',
                'description' => 'Multiple incorrect retries will cause an account to be blocked for a short amount of time.',
                'type'        => 'boolean',
                'default'     => '1'
            ),
            array(
                'id'          => 'f2fa_lockouts_maxtries',
                'name'        => 'Max tries before lock-out',
                'description' => 'Set the maximum number of log in attempts before locking an account.',
                'type'        => 'number',
                'default'     => '5'
            ),
            array(
                'id'          => 'f2fa_lockouts_expiry',
                'name'        => 'Waiting time for lock-out expiry',
                'description' => 'Set the cooldown time in minutes for a locked account to be available again.',
                'type'        => 'number',
                'default'     => '30'
            ),
            array(
                'id'          => 'f2fa_required',
                'name'        => 'Require two-factor authentication',
                'description' => 'Users without two-factor authentication will be required to activate it on log in.',
                'type'        => 'boolean',
                'default'     => '0'
            )
        );

        // Add settings section
        \add_settings_section(
            'default',      // ID tag of this section
            'General',      // Title of this section, will be rendered
            null,           // Callback to echo section description
            'f2fa-settings' // Page to display this section on
        );

        // Add each setting to the f2fa-settings default section and register it to WordPress
        foreach ($settings as $setting) {
            // Add the setting field to f2fa-settings
            \add_settings_field(
                $setting['id'],             // ID tag of this field
                $setting['name'],           // Title of this field, will be rendered
                function() use ($setting) { // Callback to render field element
                    echo $this->twig->render('setting_input.html', array(
                        'setting_name'    => $setting['id'],
                        'type'            => $setting['type'] == 'boolean' ? 'checkbox' : 'text',
                        'description'     => $setting['description'],
                        'value'           => $setting['type'] == 'boolean' ? '1' : get_option($setting['id']),
                        'extra_attribute' => $setting['type'] == 'boolean' ? checked(1, get_option($setting['id']), false) : ''
                    ));
                },
                'f2fa-settings'             // Page to display this field on
            );

            // Register the setting to WordPress
            \register_setting('f2fa-settings', $setting['id'], array(
                'type'        => $setting['type'],
                'description' => $setting['description'],
                'default'     => $setting['default']
            ));
        }
    }
}