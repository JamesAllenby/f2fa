<?php
namespace JAllenby37;

/**
 * The RegisterSettingsPage class
 * 
 * This class is responsible for adding the menu to the WordPress side-bar and rendering
 * the page contents onto the DOM.
 */
class RegisterSettingsPage {

    /** @var \Twig\Environment twig */
    private $twig;

    /**
     * RegisterOptionsPage constructor
     * 
     * @param \Twig\Environment twig
     */
    public function __construct(\Twig\Environment $twig) {
        $this->twig = $twig;
    }

    public function run() {
        // Add the f2fa-settings page to the options sub-menu
        \add_options_page(
            'F2FA Settings',                    // Page title
            'F2FA',                             // Menu title
            'manage_options',                   // Capability required
            'f2fa-settings',                    // URL slug
            array($this, 'render_options_page') // Callback function
        );
    }

    public function render_options_page() {
        // Don't render page if user does not have permission
        if (!\current_user_can('manage_options'))
            return;

        // Check if the settings have been saved
        if (isset($_GET['settings-updated']))
            \add_settings_error('f2fa_messages', 'f2fa_message', __( 'Settings Saved', 'f2fa' ), 'updated');

        // Enable output buffering
        \ob_start();
        
        // Output all forms related to F2FA
        \settings_fields('f2fa-settings');
        \do_settings_sections('f2fa-settings');
        \submit_button();

        // Store all buffered forms into $form_elements
        $form_elements = \ob_get_clean();

        // Render the final form with the $form_elements
        echo $this->twig->render('settings_f2fa.html', array(
            'title'            => \get_admin_page_title(),
            'form_elements'    => $form_elements
        ));

        // Make sure to end buffering and flush the contents
        \ob_end_flush();
    }
} 