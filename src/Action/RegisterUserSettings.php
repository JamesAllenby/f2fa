<?php
namespace JAllenby37;

/**
 * The RegisterUserSettings class.
 * 
 * This class is responsible for rendering the user settings for this plugin
 * onto the DOM.
 */
class RegisterUserSettings {

    /** @var \Twig\Environment Twig object */
    private $twig;

    /**
     * RegisterUserSettings constructor
     * 
     * @param \Twig\Environment twig
     */
    function __construct(\Twig\Environment $twig) {
        $this->twig = $twig;
    }

    public function run($user): void {
        if (\get_current_user()->ID !== $user->ID)
            if (!current_user_can('edit_users'))
                return;

        $activate_date = (int)\get_user_meta($user->ID, 'f2fa_active_date', TRUE);

        $activated = strlen(\get_user_meta($user->ID, 'f2fa_active_secret', TRUE)) ? true : false;
        $activate_date_formatted = date("d/m/Y H:i", $activate_date);

        echo $this->twig->render('user_settings.html', array(
            'activated'       => $activated,
            'activation_date' => $activate_date_formatted
        ));
    }
}