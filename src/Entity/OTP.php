<?php
namespace JAllenby37;

use \ParagonIE\ConstantTime\Base32;

class OTP {
    /**
     * Generate a 160-bit secret encoded in Base32
     */
    public static function GenerateSecret(): string {
        return Base32::encode(\random_bytes(160 / 8));
    }
}