<?php
namespace JAllenby37;

use \ParagonIE\ConstantTime\Base32;

/**
 * The TOTP class.
 * 
 * This class is responsible for maintaining time-based one-time passwords.
 */
class TOTP extends HOTP {

    /** @var int time_window */
    public $time_window;

    /**
     * TOTP constructor
     * 
     * @param string secret
     * @param int    time_window
     * @param int    digits
     */
    public function __construct(string $secret, int $time_window = 30, int $digits = 6) {
        $this->secret      = $secret;
        $this->time_window = $time_window;
        $this->digits      = $digits;
        $this->reset_time();
    }

    /**
     * Gets the one-time password for the secret
     * 
     * @return string otp
     */
    public function get_otp(): string {
        // Decode the secret from Base32
        $dsecret = Base32::decode($this->secret);

        // Generate the MAC (message authorisation code) by hashing the counter and secret
        $mac = $this->get_mac($dsecret, $this->counter, TRUE);

        // Truncate the MAC by extracting 32-bits from it
        $tmac = $this->truncate($mac);

        // Get the x-digit OTP code from the truncated MAC
        return $this->derive_otp($tmac);
    }

    /**
     * Sets the counter to the appropriate time window
     */
    public function reset_time(): void {
        $this->counter = \floor(\time() / $this->time_window);
    }
}