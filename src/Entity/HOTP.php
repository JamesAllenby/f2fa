<?php
namespace JAllenby37;

use \ParagonIE\ConstantTime\Base32;

/**
 * The HOTP class
 * 
 * This class is responsible for generating HOTP codes
 */
class HOTP {

    /** @var string secret */
    public $secret;

    /** @var int counter */
    public $counter;

    /** @var int digits */
    public $digits;

    /**
     * HOTP constructor
     * 
     * @param string secret
     * @param int    counter
     * @param int    digits
     */
    public function __construct(string $secret, int $counter, int $digits = 6) {
        $this->secret  = $secret;
        $this->counter = $counter;
        $this->digits  = $digits;
    }

    public function get_otps(int $size = 1): array {
        // Store the original counter for later restoration
        $original_counter = $this->counter;
        // Declare a new OTP code array
        $otps = array();
        // Generate the genesis OTP
        $otps[] = $this->get_otp();
        // Span the size of OTP codes
        for ($x = 1; $x <= $size; $x++) {
            $this->counter = $x;
            $otps[] = $this->get_otp();
            $this->counter = -$x;
            $otps[] = $this->get_otp();
        }
        // Reset the counter back to its original value
        $this->counter = $original_counter;
        return $otps;
    }

    /**
     * Get the OTP code based on the secret and counter
     * 
     * @return string OTP code
     */
    public function get_otp(): string {
        // Decode the secret from Base32
        $dsecret = Base32::decode($this->secret);

        // Generate the MAC (message authorisation code) by hashing the counter and secret
        $mac = $this->get_mac($dsecret, $this->counter, TRUE);

        // Truncate the MAC by extracting 32-bits from it
        $tmac = $this->truncate($mac);

        // Get the x-digit OTP code from the truncated MAC
        return $this->derive_otp($tmac);
    }

    /**
     * Hashes an integer counter with a binary secret.
     * 
     * @param bool   raw_output Return the MAC in a binary string.
     * 
     * @return string MAC (message authorisation code) string based on the counter and key.
     */
    public function get_mac(string $secret, int $counter, bool $raw_output = FALSE): string {
        /**
         * Pack counter variable into a 8-byte (64-bit) binary string
         * as per RFC-4226 Section 5.1 Symbol C.
         * See PHP Pack documentation for additional information.
         */
        $pcounter = pack('J', $counter);

        /**
         * Call HMAC-SHA1 with the packed counter as the message and the decoded
         * secret as the key to generate the message authorisation code (MAC).
         */
        return hash_hmac('SHA1', $pcounter, $secret, $raw_output);
    }

    /**
     * Dynamically truncate incoming data into a 32-bit integer.
     * 
     * @param string data Binary string to be truncated
     * 
     * @return int Truncated 32-bit integer based on $data
     */
    public function truncate(string $data): int {
        // Fetch the last 4-bits in the data and store as an offset.
        $offset = ord(substr($data, -1)) & 0xF;

        // Extract a contiguous 4-bytes from $data starting from offset
        // and store the result in the dynamic binary code variable.
        $dbc = substr($data, $offset, 4);

        // Unpack the dynamic binary code as a 32-bit unsigned integer
        // represented in big-endian format.
        $udbc = unpack('N', $dbc)[1];

        // Mask the MSB to prevent signedness errors and return the integer.
        return $udbc & 0x7FFFFFFF;
    }

    /**
     * Derive OTP code from integer
     */
    public function derive_otp(int $num): string {
        return sprintf('%\'06d', $num % pow(10, $this->digits));
    }
}