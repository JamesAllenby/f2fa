<?php
namespace JAllenby37;

/**
 * The OTPHandler class
 */
class OTPHandler {

    /** @var \Twig\Environment Twig object */
    private $twig;

    /**
     * OTPHandler constructor
     * 
     * @param \Twig\Environment twig
     */
    public function __construct(\Twig\Environment $twig) {
        $this->twig = $twig;
    }

    /**
     * Handles the OTP authentication
     * 
     * @param \WP_User|\WP_Error|null user
     * @param string                  username
     * @param string                  password
     * 
     * @return \WP_User|\WP_Error
     */
    public function run($user, string $username, string $password) {
        // Return early if user is not a WP_User
        if (!($user instanceof \WP_User))
            return $user;

        // Get the user active secret
        $active_secret = \get_user_meta($user->ID, "f2fa_active_secret", TRUE);

        // If 2FA not set for this user, continue login
        if (!strlen($active_secret))
            return $user;

        // No OTP was sent so send the OTP login page
        if (!array_key_exists('otp', $_REQUEST))
            $this->show_otp_login_page();

        // Store the recieved OTP
        $otp = $_REQUEST['otp'];

        // Create a new TOTP based on the users active secret
        $totp = new TOTP($active_secret);

        // Set the time correctly
        $totp->reset_time();

        // Compare the TOTP code to the recieved code
        // Return the user if successful
        if ($totp->get_otp() === $otp)
            return $user;

        // Code did not match, throw an error
        return new \WP_Error('f2fa_error', 'The verification code you entered was not correct, please try logging in again');
    }

    public function show_otp_login_page() {
        // Store the requested redirect if it exists, otherwise redirect to admin URL
        $redirect_to = array_key_exists('redirect_to', $_REQUEST) ? $_REQUEST['redirect_to'] : \admin_url();

        // Render the authentication page
        \login_header('Log In', 'Enter your two-factor authentication code');
        echo $this->twig->render('form_otp.html', array(
            'form_action'   => \site_url('wp-login.php', 'login_post'),
            'form_username' => $_REQUEST['log'],
            'form_password' => $_REQUEST['pwd'],
            'form_redirect' => $redirect_to
        ));
        \login_footer();

        // Exit to prevent other code from executing
        exit;
    }
}