<?php
namespace JAllenby37;

/**
 * The F2FA class.
 */
class F2FA {

    /** @var \Twig\Environment Twig object */
    private $twig;

    /**
     * F2FA constructor.
     */
    public function __construct() {
        // Initialise Twig with the templates directory
        $loader = new \Twig\Loader\FilesystemLoader(F2FA_DIR . '/templates');
        $this->twig = new \Twig\Environment($loader);
    }

    /**
     * F2FA entrypoint.
     */
    public function run() {
        // Instantiate all F2FA classes
        $register_user_setup_page = new RegisterUserSetupPage($this->twig);
        $register_user_settings   = new RegisterUserSettings($this->twig);
        $register_settings        = new RegisterSettings($this->twig);
        $register_settings_page   = new RegisterSettingsPage($this->twig);
        $otp_handler              = new OTPHandler($this->twig);

        // Add Kjua script to all admin pages
        \add_action('admin_enqueue_scripts', function() {
            \wp_enqueue_script('kjua', F2FA_URL . 'scripts/kjua.min.js');
        });

        // Register classes to actions
        \add_action('admin_init',        array($register_settings,        'run'));
        \add_action('admin_menu',        array($register_settings_page,   'run'));
        \add_action('admin_menu',        array($register_user_setup_page, 'run'));
        \add_action('show_user_profile', array($register_user_settings,   'run'));
        \add_action('edit_user_profile', array($register_user_settings,   'run'));

        // Register classes to filters
        \add_filter('authenticate',      array($otp_handler,            'run'), 90, 3);
    }

}
