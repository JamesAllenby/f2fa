# F2FA

The F2FA plugin gives WordPress administrators the ability to enforce additional security on user accounts. It aims to prevent unauthorized clients from accessing secure parts of the WordPress website it is installed on by requiring an additional password that only exists on a secure device.

## Background

There is currently an urgent need to introduce higher security standards for all the accounts that exist on Fabric platform. Fabric is a collection of themes and plugins that run on top of a multisite WordPress installation.

Websites on the internet are getting hacked more frequently than ever before, and the resulting user password dumps are being sold and traded among hackers to access peoples online profiles such as their social medias and bank accounts. We are reminded that we should *always* use a different password for every service we use. However, some people will continue to use the same password because it is convenient. This puts the user at significant risk of having their accounts taken over and vandalised.

F2FA is a free and open-source effort to improve security for Fabric and for the rest of the internet by granting its users the ability to use, copy, distribute, and modify this project without restrictions.

## Supported authentication methods

This plugin officially supports the **TOTP (Time-Based One-Time Password)** algorithm. The reason for choosing this algorithm is because it is widely adopted as the industry standard for one-time passwords. The implementation is easy and safe to create and there are many supporting clients on it for all major desktop and mobile operating systems.

## Terminology

* The word "User" refers an individual using a web browsing client to access the server running this plugin.

* The word "Server" refers to a machine that is hosting this plugin as a part of its software stack (e.g. as a WordPress plugin).

* "Pedantic mode" refers to a mode that can be set for this plugin which alters particular default values to reduce its attack surface.

* A "Shared secret" refers to a string of characters that exists on the server and a users device used to generate one-time passwords.

## Requirements

This section will discuss the desired behaviour of F2FA, including how it should behave when particular settings related to the plugin are set such as pedantic mode and enabled deferment.

### Description

When a user logs into WordPress with two-factor authentication disabled, they **MUST** be presented with a call-to-action page urging them to activate two-factor authentication on their account.

The user **MAY** activate two-factor authentication on their account at that point. If they choose not to activate it, then they **MUST** defer it to suppress the message or log out immediately.

If the user chooses to activate two-factor authentication, they should be redirected to a page that will assist them in activating it on their account. It **SHOULD** give a brief overview of what two-factor authentication and how it protects them. The page **MUST** teach the user how to find and install a two-factor authentication application on their device and how to use it, it **MAY** recommend several apps from their device vendors respective application repositories to install (e.g. App Store for iPhone, Play Store for Android). The page should include a shared secret that is generated as the page is requested.

 **QR code** for the user to scan into their preferred is **RECOMMENDED** to be present on the activation page, the QR code can be generated on the client using JavaScript as the shared secret will be sent in the same request.

If the user chooses to defer activating two-factor authentication, the plugin **MUST** create a transient variable to prevent the page from being shown for X amount of time. The transient name should be able to identify a user, so it is **RECOMMENDED** that the transient name be `f2fa-deferred-<ID>`, where `<ID>` is replaced by the ID of the user.

When a user logs into WordPress with two-factor authentication enabled, they **MUST** be presented with a second login page asking them to enter the TOTP code that the appropriate enrolled device has generated. If the user code correctly entered the code, they shall be granted access. If the user entered the code incorrectly, they will be given X amount of retries before locking them out for X amount of time.

It is **RECOMMENDED** that the max number of retries is 10 and the cooldown time after locking the account is 30 minutes. In the case that the account is locked, this **MUST** be reported and logged accordingly for further analysis.

### TOTP Time Frame

In order for TOTP to work reliably when users log in to the server. The current UNIX epoch time needs to be perfectly in-sync for both the client and the server as a new code is generated every 30 second aligned to the minute, being even 15 seconds off can result in bad user experience as a code that appears valid to them is actually invalid to the server since the server has generated another code for its perception of time.

In order to improve the user experience when signing in, the codes permitted must be checked for 30 seconds in the future and 30 seconds in the past. This will help ensure any clock drift on either the server or client side is handled and the login process will appear more seamless.

### Pedantic Mode

If pedantic mode is enabled, the user **MUST** either activate two-factor authentication on their account or log out immediately.

When the user logs into WordPress with two-factor authentication enabled, there is no relaxed TOTP time frame. The code must be entered correctly before the 30 second time block is up or the code will be rejected.

## Technical implementation notes

This section will discuss the technical implementation details for various components of this plugin.

### Generating and transmitting a shared secret

* The shared secret **MUST** be generated on the server. This is to prevent malicious clients from tampering with the contents of the database and to prevent weakening of the shared secret used.

* The shared secret **MUST** have a length of at least 160-bits. This document **RECOMMENDS** that the random method used is `random_bytes` introduced in PHP 7, as it claims to be cryptographically secure.

* It is **RECOMMENDED** that the transmission of the shared secret be done over a secure protocol such as HTTPS, TLS, IPSec, etc.

* When the shared secret is generated, it should be encoded into Base32 and stored as such. This ensures that the shared secret has readable characters in it to help with manual input on devices that do not support QR codes.

### Generating and transmitting a scannable QR code

* A QR code can be generated on the client upon the transmission of the shared secret.

* The QR code **MUST** include issuer and a label to identify the account that is logging in.

### Pedantic Mode Behaviour

Pedantic mode is an option that can be set in the administration menu of this plugin. It enforces much harsher security policies to log-in to the server. Some of these policies include:

* A stricter TOTP time frame, users must enter the code that the server expects with no room for time drifting.

* The lockout time if a TOTP code is entered incorrectly is doubled from thirty minutes to one hour.

## References

[Time-Based One-Time Password Algorithm (TOTP/RFC 6238)](https://tools.ietf.org/html/rfc6238)

[An HMAC-Based One-Time Password Algorithm (HOTP/RFC 4226)](https://tools.ietf.org/html/rfc4226)

[Keyed-Hashing for Message Authentication (HMAC/RFC 2104)](https://tools.ietf.org/html/rfc2104)

[Kjua - dynamically generated QR codes for JavaScript](https://larsjung.de/kjua/)
