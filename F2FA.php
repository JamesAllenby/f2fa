<?php
/**
 * @wordpress-plugin
 * Plugin Name: F2FA
 * Plugin URI: https://gitlab.com/jallenby37/f2fa
 * Description: A free, open-source WordPress plugin for enabling two-factor authentication on your sites
 * Version: 0.1
 * Requires at least: 5.0
 * Author: James Allenby
 * Author URI: https://gitlab.com/jallenby37
 */

use \JAllenby37\F2FA;

/**
 * If this file is called directly, abort.
 */
if (!defined('WPINC'))
    die;

/**
 * Load composer dependencies.
 */
$autoload = __DIR__ . '/vendor/autoload.php';
if (file_exists($autoload))
    require_once $autoload;

/**
 * Current plugin version.
 */
define('F2FA_VERSION', '0.1');
define('F2FA_DIR', __DIR__);
define('F2FA_URL', plugin_dir_url(__FILE__));

/**
 * Instantiate plugin and run
 */
$plugin = new F2FA;
$plugin->run();