<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class F2FATest extends TestCase
{
    /**
     * Test the functionality of packing string represented hexadecimal values
     * into ASCII representation.
     * 
     * This test is inspired by RFC-4226 Section 5.3
     */
    public function testHexadecimalPacking(): void
    {
        /**
         * @var string secret_hex Hexadecimal Secret
         */
        $secret_hex = '3132333435363738393031323334353637383930';

        /**
         * @var string secret_asc ASCII Secret
         */
        $secret_asc = '12345678901234567890';

        // Pack the hexadecimal secret into its ASCII representation
        $packed_hex = pack('H*', $secret_hex);

        // Assert that the packed hexadecimal string is equal to the ASCII secret
        $this->assertEquals($packed_hex, $secret_asc);
    }


    /**
     * Test the functionality of the HMAC-SHA1 function using a predetermined secret,
     * and an integer sequence of data to hash with their expected outputs.
     * 
     * This function is based on RFC-4226 Section 5.3
     */
    public function test_hash_hotp(): void {
        /** @var string Secret used to create message authorisation codes */
        $secret = '12345678901234567890';

        /** @var array An associative array of counters to their respective MAC */
        $hmac_message_result = array(
            0 => 'cc93cf18508d94934c64b65d8ba7667fb7cde4b0',
            1 => '75a48a19d4cbe100644e8ac1397eea747a2d33ab',
            2 => '0bacb7fa082fef30782211938bc1c5e70416ff44',
            3 => '66c28227d03a2d5529262ff016a1e6ef76557ece',
            4 => 'a904c900a64b35909874b33e61c5938a8e15ed1c',
            5 => 'a37e783d7b7233c083d4f62926c7a25f238d0316',
            6 => 'bc9cd28561042c83f219324d3c607256c03272ae',
            7 => 'a4fb960c0bc06e1eabb804e5b397cdc4b45596fa',
            8 => '1b3c89f65e6c9e883012052823443f048b4332db',
            9 => '1637409809a679dc698207310c8c7fc07290d9e5'
        );

        // Generate each MAC using the secret and count and compare it to the expected result
        foreach ($hmac_message_result as $count => $result) {
            $hotp = new JAllenby37\HOTP($secret, $count);
            $mac = $hotp->get_mac();
            $this->assertEquals($mac, $result);
        }
    }

    public function test_truncate_mac(): void
    {
        /** @var string Secret used to create message authorisation codes */
        $secret = '12345678901234567890';

        /** @var int    Counter */
        $counter = 0;

        /**
         * @var array An associative array of MACs mapped to tuples of expected values.
         * Tuple definition:
         * [0]: Truncated value in integer form
         * [2]: Final 6-digit OTP code
         */
        $mac_truncate_result = array(
            'cc93cf18508d94934c64b65d8ba7667fb7cde4b0' => array(1284755224, '755224'),
            '75a48a19d4cbe100644e8ac1397eea747a2d33ab' => array(1094287082, '287082'),
            '0bacb7fa082fef30782211938bc1c5e70416ff44' => array( 137359152, '359152'),
            '66c28227d03a2d5529262ff016a1e6ef76557ece' => array(1726969429, '969429'),
            'a904c900a64b35909874b33e61c5938a8e15ed1c' => array(1640338314, '338314'),
            'a37e783d7b7233c083d4f62926c7a25f238d0316' => array( 868254676, '254676'),
            'bc9cd28561042c83f219324d3c607256c03272ae' => array(1918287922, '287922'),
            'a4fb960c0bc06e1eabb804e5b397cdc4b45596fa' => array(  82162583, '162583'),
            '1b3c89f65e6c9e883012052823443f048b4332db' => array( 673399871, '399871'),
            '1637409809a679dc698207310c8c7fc07290d9e5' => array( 645520489, '520489')
        );

        foreach ($mac_truncate_result as $mac => $results) {
            // Create HOTP object
            $hotp = new JAllenby37\HOTP($secret, $counter++);

            // Test if the MAC is generated correctly
            $generated_mac = $hotp->get_mac();
            $this->assertEquals($generated_mac, $mac);

            // Test if the truncated integer value is expected
            $tmac = pack('H*', $generated_mac);
            $truncated = $hotp->truncate($tmac);
            $this->assertEquals($truncated, $results[0]);

            $otp = $hotp->derive_otp($truncated);
            $this->assertEquals($otp, $results[1]);
        }
    }
}